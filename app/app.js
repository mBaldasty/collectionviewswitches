import Vue from 'nativescript-vue'

import Home from './components/Home'

Vue.registerElement('HTMLLabel', () => require('@nativescript-community/ui-label').Label);
import SwitchPlugin from '@nativescript-community/ui-material-switch/vue';
Vue.use(SwitchPlugin);
import CollectionView from '@nativescript-community/ui-collectionview/vue';
Vue.use(CollectionView);

new Vue({
  render: (h) => h('frame', [h(Home)]),
}).$start()
